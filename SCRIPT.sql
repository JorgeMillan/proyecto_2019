create database if not exists pccomponentes_proyecto;
use pccomponentes_proyecto;

/*
drop database pccomponentes_proyecto;
*/

create table if not exists proveedores (
	id int primary key auto_increment,
    nombre_empresa varchar(50),
    nif varchar(9),
    direccion_empresa varchar(50),
    codigo_postal_empresa varchar(5),
    telefono_empresa varchar(9),
    email_empresa varchar(50),
    fax varchar(9)
);

create table if not exists piezas (
	id_pieza int primary key auto_increment,
    nombre_pieza varchar(50),
    precio_pieza double,
    stock int,
    direccion_almacen varchar(50),
    descripcion1 varchar(50),
    descripcion2 varchar(50),
    descripcion3 varchar(50),
    urlFoto varchar(100),
    id_proveedor int,
    foreign key (id_proveedor) references proveedores (id) on delete cascade
);

create table if not exists pc (
	id int primary key auto_increment,
    marca varchar(50),
    modelo varchar(50),
    gaming boolean,
    oficina boolean,
    streaming boolean,
    servidor boolean
);

create table if not exists factura (
	id int primary key auto_increment,
    nombre_cliente varchar(50),
    dni varchar(50),
    edad int,
    fecha_compra date,
    metodo_pago varchar(50),
    email_cliente varchar(50),
    telefono_cliente varchar(50),
    id_pc int,
    foreign key (id_pc) references pc(id) on delete cascade
);


create table if not exists pc_pieza (
	id_pc int,
    id_pieza int,
    primary key(id_pc, id_pieza),
	foreign key (id_pc)  references pc(id) on delete cascade,
	foreign key (id_pieza)  references piezas(id) on delete cascade
);


/*Query que usa el reportProyecto2*/
/*
select *
from   pccomponentes_proyecto.factura fac,
	   pccomponentes_proyecto.pc pc,
       pccomponentes_proyecto.piezas pie,
       pccomponentes_proyecto.pc_pieza ppie
       
where  fac.id = $P{facturaID} and
	   fac.id_pc = pc.id and
       ppie.id_pc = pc.id and
       ppie.id_Pieza = pie.id_pieza;
*/

/*Query que usa el reportProyecto*/
/*
select pro.id,
	   pro.nombre_empresa,
       pie.id_pieza,
       pie.nombre_pieza
       
from   pccomponentes_proyecto.proveedores pro,
	   pccomponentes_proyecto.piezas pie
       
where  pro.id = pie.id_proveedor
order  by pro.id;
*/





