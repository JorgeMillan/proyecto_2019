package com.jmillan.mvc;

import com.jmillan.base.Factura;
import com.jmillan.base.Pc;
import com.jmillan.base.Pieza;
import com.jmillan.base.Proveedor;
import com.jmillan.util.HibernateUtil;

import javax.swing.*;
import java.util.List;

public class Modelo {

    Vista vista;

    public void conectar() {
        HibernateUtil.buildSessionFactory();
        HibernateUtil.openSession();
    }

    public void desconectar() {
        HibernateUtil.closeSessionFactory();
    }

    //Métodos proveedor

    /**
     * Lista los proveedor desde base da datos
     * @return lista de proveedores
     */
    @SuppressWarnings("JpaQlInspection")
    public List<Proveedor> listarProveedores() {
        try {
            return HibernateUtil.getCurrentSession().createQuery("from Proveedor").setHint("javax.persistence.query.timeout", 10).getResultList();
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Asegurese de haber creado la base de datos primero y haber iniciado el servidor. Para más información consulte el manual","Error",0);
            System.exit(0);
            return null;
        }
    }

    /**
     * Guarda un proveedor en base de datos
     * @param proveedor proveedor a buscar
     */
    public void guardarProveedor(Proveedor proveedor) {
        HibernateUtil.getCurrentSession().beginTransaction();
        HibernateUtil.getCurrentSession().saveOrUpdate(proveedor);
        HibernateUtil.getCurrentSession().getTransaction().commit();
    }

    /**
     * Elimina un proveedor en base de datos
     * @param proveedor proveedor a eliminar
     */
    public void eliminarProveedor(Proveedor proveedor) {
        HibernateUtil.getCurrentSession().beginTransaction();
        HibernateUtil.getCurrentSession().delete(proveedor);
        HibernateUtil.getCurrentSession().getTransaction().commit();
    }
    //Métodos piezas

    /**
     * Lista las piezas desde base da datos
     * @return lista de piezas
     */
    @SuppressWarnings("JpaQlInspection")
    public List<Pieza> listarPiezas() {
        return HibernateUtil.getCurrentSession().createQuery("from Pieza").getResultList();
    }

    /**
     * Guarda una pieza en base de datos
     * @param pieza pieza a guardar
     */
    public void guardarPieza(Pieza pieza) {
        HibernateUtil.getCurrentSession().beginTransaction();
        HibernateUtil.getCurrentSession().saveOrUpdate(pieza);
        HibernateUtil.getCurrentSession().getTransaction().commit();
    }

    /**
     * Elimina una pieza en base de datos
     * @param pieza pieza a eliminar
     */
    public void eliminarPieza(Pieza pieza) {
        HibernateUtil.getCurrentSession().beginTransaction();
        HibernateUtil.getCurrentSession().delete(pieza);
        HibernateUtil.getCurrentSession().getTransaction().commit();
    }
    //Métodos PC

    /**
     * Lista los pc desde base de datos
     * @return lista de pcs
     */
    @SuppressWarnings("JpaQlInspection")
    public List<Pc> listarPc() {
        return HibernateUtil.getCurrentSession().createQuery("from Pc").getResultList();
    }

    /**
     * Guarda un pc en base de datos
     * @param pc pc a guardar
     */
    public void guardarPc(Pc pc) {
        HibernateUtil.getCurrentSession().beginTransaction();
        HibernateUtil.getCurrentSession().saveOrUpdate(pc);
        HibernateUtil.getCurrentSession().getTransaction().commit();
    }

    /**
     * Elimina un pc en base de datos
     * @param pc pc a borrar
     */
    public void eliminarPc(Pc pc) {
        HibernateUtil.getCurrentSession().beginTransaction();
        HibernateUtil.getCurrentSession().delete(pc);
        HibernateUtil.getCurrentSession().getTransaction().commit();
    }

    /**
     * Lista las piezas de un pc en base de datos
     * @param pc pc del que se quieren las piezas
     * @return piezas del pc
     */
    public List<Pieza> listarPcPiezas(Pc pc) {
        return pc.getPiezas();
    }

    //Métodos factura

    /**
     * lista una factura en base de datos
     * @return lista de facturas
     */
    @SuppressWarnings("JpaQlInspection")
    public List<Factura> listarFacturas() {
        return HibernateUtil.getCurrentSession().createQuery("from Factura").getResultList();
    }

    /**
     * Guarda una factura en base de datos
     * @param factura factura a guardar
     */
    public void guardarFactura(Factura factura) {
        HibernateUtil.getCurrentSession().beginTransaction();
        HibernateUtil.getCurrentSession().saveOrUpdate(factura);
        HibernateUtil.getCurrentSession().getTransaction().commit();
    }

    /**
     * Elimina una factura en base de datos
     * @param factura factura a eliminar
     */
    public void eliminarFactura(Factura factura) {
        HibernateUtil.getCurrentSession().beginTransaction();
        HibernateUtil.getCurrentSession().delete(factura);
        HibernateUtil.getCurrentSession().getTransaction().commit();
    }
}
