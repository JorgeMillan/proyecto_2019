package com.jmillan.mvc;

import com.jmillan.Libreria.Libreria;
import com.jmillan.base.Factura;
import com.jmillan.base.Pc;
import com.jmillan.base.Pieza;
import com.jmillan.base.Proveedor;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.commons.io.FileUtils;

import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class Controlador implements ListSelectionListener, ActionListener, KeyListener, MouseListener, FocusListener {

    Vista vista;
    Modelo modelo;
    JFileChooser fileChooser = new JFileChooser();
    File rutaImagen;


    //Para usar en los test
    public Controlador() {}

    /**
     * Constructor de la clase
     * @param vista Vista a crear para la interfaz de usuario.
     * @param modelo Clase Modelo que implementa operaciones con los distintos objetos y la base de datos.
     */
    public Controlador(Vista vista, Modelo modelo) {

        FileNameExtensionFilter filter = new FileNameExtensionFilter("IMAGEN","jpg","jpeg","png");
        fileChooser.addChoosableFileFilter(filter);

        try {
            Libreria libreria = new Libreria();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        this.vista = vista;
        this.modelo = modelo;



        modelo.conectar();
        anadirListeners();
        refrescarListas();
        listarPc();
        listarUsuarios();
        ActualizarTablas actualizarTablas = new ActualizarTablas();
        Thread thread = new Thread(actualizarTablas);
        thread.start();
    }

	/**
	 * ActionListener de los dioferentes botones de la vista.
	 * @param e Objeto que origina el evento.
	*/
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            //Panel proveedorModificar
            case "proveedorGuardar":
                if (comprobarProveedor()) guardarProveedor();
                break;
            case "proveedorModificar":
                if (comprobarProveedor()) {
                    Proveedor proveedorModificar = (Proveedor) vista.listProveedores.getSelectedValue();
                    modificarProveedor(proveedorModificar);
                }
                break;
            case "proveedorEliminar":
                eliminarProveedor();
                break;
            //Panel piezas
            case "piezaGuardar":
                if (vista.lbPiezasFoto.getIcon() == null) {
                    JOptionPane.showMessageDialog(
                            null,"Selecciona una foto","Error", JOptionPane.ERROR_MESSAGE);
                }else {
                    comprobarComaoPunto(vista.txtPiezasPrecio);
                    guardarPieza();
                }
                break;
            case "piezaModificar":
                //System.err.println(rutaImagen.getName());
                Pieza piezaModificar = (Pieza) vista.listPiezas.getSelectedValue();
                comprobarComaoPunto(vista.txtPiezasPrecio);
                modificarPieza(piezaModificar);
                break;
            case "piezaEliminar":
                eliminarPieza();
                break;
            //Panel Pc
            case "pcGuardar":
                if (!vista.txtPcModelo.getText().isEmpty() || !vista.txtPcMarca.getText().isEmpty()) guardarPc();
                else JOptionPane.showMessageDialog(null,"Marca o medelo está vacio","Error",0);
                break;
            case "pcModificar":
                Pc pcModificar = (Pc) vista.listPc.getSelectedValue();
                if (!vista.txtPcModelo.getText().isEmpty() || !vista.txtPcMarca.getText().isEmpty()) modificarPc(pcModificar);
                else JOptionPane.showMessageDialog(null,"Marca o medelo está vacio","Error",0);
                break;
            case "pcEliminar":
                eliminarPc();
                break;
            case "pcGuardarPieza":

                guardarPiezaPc();
                break;
            case "pcEliminarPieza":
                eliminarPiezaPc();
                break;
            //Panel Facturas
            case "facturaGuardar":
                if (comprobarFactura()) guardarFactura();
                break;
            case "facturaModificar":

                if ((vista.dpFacturaFecha.getDate().isAfter(LocalDate.now())) && comprobarFactura()) {
                    JOptionPane.showMessageDialog(
                            null,"No se puede dar de alta una factura con fecha superior a la actual",
                            "Error",JOptionPane.ERROR_MESSAGE
                    );
                } else {
                    Factura facturaModificar = (Factura) vista.listFactura.getSelectedValue();
                    modificarFactura(facturaModificar);
                }
                break;
            case "facturaEliminar":
                eliminarFactura();
                break;
            case "guardarUsuario":
                if (!vista.txtUsuariosNombre.getText().isEmpty()) {
                    if (compararPassword()) {
                        guardarUsuario();
                    }
                } else JOptionPane.showMessageDialog(null,"Nombre vacio","Error",0);
                break;
            case "cerrarSesion":
                Vista.frame.dispose();
                Login login = new Login();
                break;
            case "buscar":
                listarBusqueda(vista.txtBusqueda.getText());
                break;
            case "facturaGenerar":
                try {
                    facturaGenerar();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "informePiezas":
                try {
                    informePiezas();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;


        }
    }




    @Override
    public void keyTyped(KeyEvent e) {}
    @Override
    public void keyPressed(KeyEvent e) {}
    /**
	 * KeyListeners para diferentes elementos de la interfaz.
	 * Solo se usa el KeyReleased
	*/
	@Override
    public void keyReleased(KeyEvent e) {
	    if (e.getSource().equals(vista.txtProveedoresCodigoPostal)) {
            if (Character.isLetter(e.getKeyChar())) {
                vista.txtProveedoresCodigoPostal.setText(vista.txtProveedoresCodigoPostal.getText().replace(e.getKeyChar(),' ').trim());
            }
        }
        if (e.getSource().equals(vista.txtFacturaEdad)) {
            if (Character.isLetter(e.getKeyChar())) {
                vista.txtFacturaEdad.setText(vista.txtFacturaEdad.getText().replace(e.getKeyChar(),' ').trim());
            }
        }
        if (e.getSource().equals(vista.txtFacturaTelefono)) {
            if (Character.isLetter(e.getKeyChar())) {
                vista.txtFacturaTelefono.setText(vista.txtFacturaTelefono.getText().replace(e.getKeyChar(),' ').trim());
            }
        }
        if (e.getSource().equals(vista.txtPiezasPrecio)) {
            if (Character.isLetter(e.getKeyChar())) {
                vista.txtPiezasPrecio.setText(vista.txtPiezasPrecio.getText().replace(e.getKeyChar(),' ').trim());
            }
        }
        if (e.getSource().equals(vista.txtPiezasStock)) {
            if (Character.isLetter(e.getKeyChar())) {
                vista.txtPiezasStock.setText(vista.txtPiezasStock.getText().replace(e.getKeyChar(),' ').trim());
            }
        }
        if (e.getSource().equals(vista.txtProveedoresTelefono)) {
            if (Character.isLetter(e.getKeyChar())) {
                vista.txtProveedoresTelefono.setText(vista.txtProveedoresTelefono.getText().replace(e.getKeyChar(),' ').trim());
            }
        }
        if (e.getSource().equals(vista.txtProveedoresFax)) {
            if (Character.isLetter(e.getKeyChar())) {
                vista.txtProveedoresFax.setText(vista.txtProveedoresFax.getText().replace(e.getKeyChar(),' ').trim());
            }
        }
        if (e.getSource().equals(vista.txtProveedoresFax)) {
            if (Character.isLetter(e.getKeyChar())) {
                vista.txtProveedoresFax.setText(vista.txtProveedoresFax.getText().replace(e.getKeyChar(),' ').trim());
            }
        }
        if (e.getSource().equals(vista.txtFacturaTelefono)) {
            if (Character.isLetter(e.getKeyChar())) {
                vista.txtFacturaTelefono.setText(vista.txtFacturaTelefono.getText().replace(e.getKeyChar(),' ').trim());
            }
        }

        if (e.getSource().equals(vista.txtProveedoresFax)) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                vista.txtProveedoresFax.setText(vista.txtProveedoresFax.getText().substring(0,8));
                guardarProveedor();
            }
        }
        if (e.getSource().equals(vista.cbPiezasProveedor)) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                guardarPieza();
            }
        }
        if (e.getSource().equals(vista.checkBoxPcServidor)) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                guardarPc();
            }
        }
        if (e.getSource().equals(vista.listPcPiezasDisponibles)) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                guardarPiezaPc();
            }
        }
        if (e.getSource().equals(vista.cbFacturaPc)) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                guardarFactura();
            }
        }
        if (e.getSource().equals(vista.listProveedores)) {
            if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                eliminarProveedor();
            }
        }
        if (e.getSource().equals(vista.listPiezas)) {
            if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                eliminarPieza();
            }
        }
        if (e.getSource().equals(vista.listPc)) {
            if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                eliminarPc();
            }
        }
        if (e.getSource().equals(vista.listPcPiezas)) {
            if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                eliminarPiezaPc();
            }
        }
        if (e.getSource().equals(vista.listFactura)) {
            if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                eliminarFactura();
            }
        }
        if (e.getSource().equals(vista.txtBusqueda)) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                listarBusqueda(vista.txtBusqueda.getText());
            }
        }
    }
	/**
	 * MouseListener para los diferentes elemntos de la interfaz, 
	 * sobretodo las listas.
	 * @param e Objeto que desencadena el listener
	*/
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource().equals(vista.listFactura)) {
            if (e.getClickCount() == 2) {
                try {
                    facturaGenerar();
                } catch (SQLException e1) {
                    JOptionPane.showMessageDialog(null,"Se ha producido un error al generar la factura. Comprueba la conexión con la base de datos","ERROR",0);

                }
            }
        }
        if (e.getSource().equals(vista.listPcPiezasDisponibles)) {
            if (e.getClickCount() == 2) {
                guardarPiezaPc();
            }
        }
        if (e.getSource().equals(vista.listPcPiezas)) {
            if (e.getClickCount() == 2) {
                eliminarPiezaPc();
            }
        }
        if (e.getSource().equals(vista.lbPiezasFoto)) {
            if (e.getClickCount() == 2) {
                if (fileChooser.showDialog(null,null) == JFileChooser.APPROVE_OPTION) {
                    System.out.println(fileChooser.getSelectedFile().getAbsolutePath());
                    copy(fileChooser.getSelectedFile());
                    vista.lbPiezasFoto.setText("");
                    vista.lbPiezasFoto.setIcon(new ImageIcon(
                            new ImageIcon("\\PCComponentes_Espana\\img\\"+fileChooser.getSelectedFile().getName())
                                    .getImage().getScaledInstance(60,60,Image.SCALE_SMOOTH)));

                }
            }
        }
        if (e.getSource().equals(vista.listBusqueda)) {
            if (e.getClickCount() == 2) {
                if (vista.rbBusquedaProveedores.isSelected()) {
                    vista.tabbedPane1.setSelectedIndex(3);
                    Proveedor proveedor = (Proveedor) vista.listBusqueda.getSelectedValue();
                    rellenarProveedor(proveedor);

                }
                if (vista.rbBusquedaPiezas.isSelected()) {
                    vista.tabbedPane1.setSelectedIndex(2);
                    Pieza pieza = (Pieza) vista.listBusqueda.getSelectedValue();
                    rellenarPieza(pieza);

                }
                if (vista.rbBusquedaPc.isSelected()) {
                    vista.tabbedPane1.setSelectedIndex(1);
                    Pc pc = (Pc) vista.listBusqueda.getSelectedValue();
                    rellenarPc(pc);

                }
                if (vista.rbBusquedaFacturas.isSelected()) {
                    vista.tabbedPane1.setSelectedIndex(0);
                    Factura factura = (Factura) vista.listBusqueda.getSelectedValue();
                    rellenarFactura(factura);

                }
            }
        }
    }
    @Override
    public void mousePressed(MouseEvent e) {}
    @Override
    public void mouseReleased(MouseEvent e) {}
    @Override
    public void mouseEntered(MouseEvent e) {}
    @Override
    public void mouseExited(MouseEvent e) {}

	/**
	 * ListListener para las diferentes listas de la interfaz.
	 * @param e Objeto que desencadena el listener
	*/
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()) {
            //Panel proveedores
            if (e.getSource() == vista.listProveedores) {
                Proveedor proveedor = (Proveedor) vista.listProveedores.getSelectedValue();
                rellenarProveedor(proveedor);
            }
            //Panel piezas
            if (e.getSource() == vista.listPiezas) {
                Pieza pieza = (Pieza) vista.listPiezas.getSelectedValue();
                rellenarPieza(pieza);

            }
            //Panel Pc
            if (e.getSource() == vista.listPc) {
                Pc pc = (Pc) vista.listPc.getSelectedValue();
                rellenarPc(pc);
            }
            //Panel de Factura
            if (e.getSource() == vista.listFactura) {
                Factura factura = (Factura) vista.listFactura.getSelectedValue();
                rellenarFactura(factura);
            }
            if (e.getSource() == vista.listUsuarios) {
                try {
                    rellenarUsuarios();
                } catch (IOException e1) {
                    JOptionPane.showMessageDialog(null,"Se ha producido un error al acceder al archivo usuarios.txt","ERROR",0);

                }
            }

        }

    }



    @Override
    public void focusGained(FocusEvent e) {

    }
	
	/**
	 * FocusListener para los diferentes elementos de la vista,
	 * generalmente para comprobar entradas del usuario.
	 * @param e Objeto que desencadena el listener.
	*/
    @Override
    public void focusLost(FocusEvent e) {
        if (e.getSource().equals(vista.txtProveedoresFax)) {
            comprobarCampoLongitud(vista.txtProveedoresFax, "El fax debe tener 9 caracteres", 9);
        }else if (e.getSource().equals(vista.txtProveedoresCodigoPostal)) {
            comprobarCampoLongitud(vista.txtProveedoresCodigoPostal, "El código postal debe tener 5 caracteres", 5);
        } else if (e.getSource().equals(vista.txtProveedoresNif)) {
            comprobarCampoLongitud(vista.txtProveedoresNif,"El NIF debe tener 9 caracteres", 9);
            comprobarUltimoEsLetra(vista.txtProveedoresNif, "El NIF debe acabar con una letra");
            comprobarNoMasDeDosLetras(vista.txtProveedoresNif,"El NIF no debe contener más de dos letras");
        } else if (e.getSource().equals(vista.txtProveedoresTelefono)) {
            comprobarCampoLongitud(vista.txtProveedoresTelefono, "El teléfono debe tener 9 números", 9);
        } else if (e.getSource().equals(vista.txtFacturaTelefono)) {
            comprobarCampoLongitud(vista.txtFacturaTelefono, "El teléfono debe tener 9 números", 9);
        } else if (e.getSource().equals(vista.txtFacturaDni)) {
            comprobarCampoLongitud(vista.txtFacturaDni,"El DNI debe tener 9 caracteres", 9);
            comprobarUltimoEsLetra(vista.txtFacturaDni, "El DNI debe acabar con una letra");
            comprobarNoMasDeDosLetras(vista.txtFacturaDni,"El DNI no debe contener más de dos letras");
        } else if (e.getSource().equals(vista.txtFacturaEdad)) {
            comprobarCampoLongitud(vista.txtFacturaEdad,"La edad no debe tener más de 3 caracteres", 3);
            if (Integer.parseInt(vista.txtFacturaEdad.getText()) >= 120) {
                vista.txtFacturaEdad.setText("120");
            }

        } else if (e.getSource().equals(vista.txtPiezasPrecio)) {
            vista.txtPiezasPrecio.setText(vista.txtPiezasPrecio.getText().replace(",","."));
        }
    }

	
	/**
	 * Refresca las listas de la vista
	*/
    private void refrescarListas() {
        //Panel proveedor
        listarProveedores();
        //Panel piezas
        listarCbPiezasProveedor();
        listarPiezas();
        //listarPcPiezas();
        //Panel facturas
        listarFacturas();
        listarCbFacturasPc();

    }

	/**
	 * Lista los PC disponibles en el ComboBox del panel Facturas
	*/
    private void listarCbFacturasPc() {
        List<Pc> listaPc = modelo.listarPc();
        vista.dlmCbFacturaPc.removeAllElements();
        for (Pc pc : listaPc) {
            vista.dlmCbFacturaPc.addElement(pc);
        }
    }
	/**
	 * Lista los usuario en la lista del panel Usuarios
	*/
    private void listarUsuarios() {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("usuarios.txt"));
            vista.dlmListUsuarios.clear();
            for (String string : properties.stringPropertyNames()) {
                if (vista.usuario.equals("admin"))vista.dlmListUsuarios.addElement(string);
                else if (string.equals(vista.usuario))vista.dlmListUsuarios.addElement(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
	/**
	 * Lista las Facturas en la lista del panel Facturas
	*/
    private void listarFacturas() {
        List<Factura> listaFacturas = modelo.listarFacturas();
        vista.dlmListFactura.clear();
        for (Factura factura : listaFacturas) {
            vista.dlmListFactura.addElement(factura);
        }
    }

	/**
	* Lista las piezas disponibles en el panel PC
	*/
    private void listarPcPiezasDisponibles() {
        try {
            List<Pieza> piezas = modelo.listarPiezas();
            Pc pc = (Pc) vista.listPc.getSelectedValue();
            piezas.removeAll(pc.getPiezas());
            vista.dlmListPcPiezasDisponibles.clear();
            for (Pieza pieza : piezas) {
                vista.dlmListPcPiezasDisponibles.addElement(pieza);
            }
        }catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
	/**
	 * Lista las piezas de un PC seleccionado en las lista de PCs del panel PC 
	 */
    private void listarPcPiezas() {
        try {
            List<Pieza> pcPiezasList = modelo.listarPcPiezas((Pc) vista.listPc.getSelectedValue());
            vista.dlmListPcPieza.clear();
            for (Pieza pieza : pcPiezasList) {
                vista.dlmListPcPieza.addElement(pieza);
            }
        }catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
	/**
	 * Lista los PC disponibles en la lista del panel PC
	 */
    private void listarPc() {
        List<Pc> pcList = modelo.listarPc();
        vista.dlmListPc.clear();
        for (Pc pc : pcList) {
            vista.dlmListPc.addElement(pc);
        }
    }
	/**
	 * Lista en el ComboBox del panel piezas los proveedores
	 */
    private void listarCbPiezasProveedor() {
        List<Proveedor> proveedores = modelo.listarProveedores();
        vista.dlmCbPiezasProveedor.removeAllElements();
        for (Proveedor proveedor : proveedores) {
            vista.dlmCbPiezasProveedor.addElement(proveedor);
        }
    }
	/**
	 * Lista los provedores en la lista del panel proveedores 
	 */
    private void listarProveedores() {
        List<Proveedor> proveedorList = modelo.listarProveedores();
        vista.dlmListProveedores.clear();
        for (Proveedor proveedor : proveedorList) {
            vista.dlmListProveedores.addElement(proveedor);
        }
    }
	/**
	 * Lista las piezas disponibles en la lista del panel Piezas
	 */
    private void listarPiezas() {
        List<Pieza> piezas = modelo.listarPiezas();
        vista.dlmListPiezas.clear();
        for (Pieza pieza : piezas) {
            vista.dlmListPiezas.addElement(pieza);
        }
    }
	/**
	 * Lista en la lista del panel Busqueda según lo que se haya buscado
	 */
    private void listarBusqueda(String txtBusqueda) {
        if (vista.rbBusquedaProveedores.isSelected()) {
            vista.dlmListBusquedaProveedor = new DefaultListModel<Proveedor>();

            vista.listBusqueda.setModel(vista.dlmListBusquedaProveedor);
            List<Proveedor> proveedores = modelo.listarProveedores();
            vista.dlmListBusquedaProveedor.clear();
            for (Proveedor proveedor : proveedores) {
                if (proveedor.getNombreEmpresa().equalsIgnoreCase(txtBusqueda)
                    || proveedor.getNif().equalsIgnoreCase(txtBusqueda)
                    || proveedor.getDireccionEmpresa().equalsIgnoreCase(txtBusqueda)
                    || proveedor.getCodigoPostalEmpresa().equalsIgnoreCase(txtBusqueda)
                    || proveedor.getTelefonoEmpresa().equalsIgnoreCase(txtBusqueda)
                    || proveedor.getEmailEmpresa().equalsIgnoreCase(txtBusqueda)
                    || proveedor.getFax().equalsIgnoreCase(txtBusqueda)) {
                        vista.dlmListBusquedaProveedor.addElement(proveedor);
                }
            }
        }
        if (vista.rbBusquedaPiezas.isSelected()) {
            vista.dlmListBusquedaPieza = new DefaultListModel<Pieza>();
            vista.listBusqueda.setModel(vista.dlmListBusquedaPieza);

            List<Pieza> piezas = modelo.listarPiezas();
            vista.dlmListBusquedaPieza.clear();
            for (Pieza pieza : piezas) {
                if (pieza.getNombrePieza().equalsIgnoreCase(txtBusqueda)
                    || String.valueOf(pieza.getPrecioPieza()).equalsIgnoreCase(txtBusqueda)
                    || String.valueOf(pieza.getStock()).equalsIgnoreCase(txtBusqueda)
                    || pieza.getDireccionAlmacen().equalsIgnoreCase(txtBusqueda)
                    || pieza.getDescripcion1().equalsIgnoreCase(txtBusqueda)
                    || pieza.getDescripcion2().equalsIgnoreCase(txtBusqueda)
                    || pieza.getDescripcion3().equalsIgnoreCase(txtBusqueda)
                    || pieza.getUrl().contains(txtBusqueda)) {
                        vista.dlmListBusquedaPieza.addElement(pieza);
                }
            }
        }
        if (vista.rbBusquedaPc.isSelected()) {
            vista.dlmListBusquedaPc = new DefaultListModel<Pc>();
            vista.listBusqueda.setModel(vista.dlmListBusquedaPc);

            List<Pc> pcs = modelo.listarPc();
            vista.dlmListBusquedaPc.clear();
            for (Pc pc : pcs) {
                if (pc.getMarca().equals(txtBusqueda)
                    || pc.getModelo().equalsIgnoreCase(txtBusqueda)) {
                        vista.dlmListBusquedaPc.addElement(pc);
                } else if (txtBusqueda.equalsIgnoreCase("gaming")
                            && pc.isGaming()) {
                    vista.dlmListBusquedaPc.addElement(pc);
                } else if (txtBusqueda.equalsIgnoreCase("servidor")
                            && pc.isServidor()) {
                    vista.dlmListBusquedaPc.addElement(pc);
                } else if (txtBusqueda.equalsIgnoreCase("oficina")
                            && pc.isOficina()) {
                    vista.dlmListBusquedaPc.addElement(pc);
                } else if (txtBusqueda.equalsIgnoreCase("streaming")
                            && pc.isStreaming()) {
                    vista.dlmListBusquedaPc.addElement(pc);
                }
            }
        }
        if (vista.rbBusquedaFacturas.isSelected()) {
            vista.dlmListBusquedaFactura = new DefaultListModel<Factura>();
            vista.listBusqueda.setModel(vista.dlmListBusquedaFactura);

            List<Factura> facturas = modelo.listarFacturas();
            vista.dlmListBusquedaFactura.clear();
            for (Factura factura : facturas) {
                if (factura.getNombreFactura().equalsIgnoreCase(txtBusqueda)
                    || factura.getDni().equalsIgnoreCase(txtBusqueda)
                    || String.valueOf(factura.getEdad()).equalsIgnoreCase(txtBusqueda)
                    || factura.getFechaCompra().toString().equalsIgnoreCase(txtBusqueda)
                    || factura.getMetodoPago().equalsIgnoreCase(txtBusqueda)
                    || factura.getEmailFactura().equalsIgnoreCase(txtBusqueda)
                    || factura.getTelefonoFactura().equalsIgnoreCase(txtBusqueda)) {
                        vista.dlmListBusquedaFactura.addElement(factura);
                }
            }
        }
    }

	/**
	 * Guarda un proveedor en la base de datos
	 */
    private void guardarProveedor() {
            modelo.guardarProveedor(new Proveedor(
                    vista.txtProveedoresNombre.getText(),
                    vista.txtProveedoresNif.getText(),
                    vista.txtProveedoresDireccion.getText(),
                    vista.txtProveedoresCodigoPostal.getText(),
                    vista.txtProveedoresTelefono.getText(),
                    vista.txtProveedoresEmail.getText(),
                    vista.txtProveedoresFax.getText()
            ));
            refrescarListas();
    }

    /**
     * Es llamado cuando se elige un registro en la lista de proveedores, pinta en pantalla
     * los campos del seleccionado
     * @param proveedorModificar
     */
    private void modificarProveedor(Proveedor proveedorModificar) {
        proveedorModificar.setNombreEmpresa(vista.txtProveedoresNombre.getText());
        proveedorModificar.setCodigoPostalEmpresa(vista.txtProveedoresCodigoPostal.getText());
        proveedorModificar.setDireccionEmpresa(vista.txtProveedoresDireccion.getText());
        proveedorModificar.setEmailEmpresa(vista.txtProveedoresEmail.getText());
        proveedorModificar.setFax(vista.txtProveedoresFax.getText());
        proveedorModificar.setNif(vista.txtProveedoresNif.getText());
        proveedorModificar.setTelefonoEmpresa(vista.txtProveedoresTelefono.getText());
        modelo.guardarProveedor(proveedorModificar);
        refrescarListas();
    }

    /**
     * Es llamado cuando se elige un registro en la lista de facturas, pinta en pantalla
     * los campos del seleccionado
     * @param facturaModificar
     */
    private void modificarFactura(Factura facturaModificar) {
        facturaModificar.setNombreFactura(vista.txtFacturaNombre.getText());
        facturaModificar.setDni(vista.txtFacturaDni.getText());
        facturaModificar.setEdad(Integer.parseInt(vista.txtFacturaEdad.getText()));
        facturaModificar.setFechaCompra(Date.valueOf(vista.dpFacturaFecha.getDate()));
        facturaModificar.setMetodoPago(vista.txtFacturaPago.getText());
        facturaModificar.setEmailFactura(vista.txtFacturaEmail.getText());
        facturaModificar.setTelefonoFactura(vista.txtFacturaTelefono.getText().substring(0,8));
        facturaModificar.setPc((Pc) vista.cbFacturaPc.getSelectedItem());

        modelo.guardarFactura(facturaModificar);
        refrescarListas();
    }

    /**
     * Es llamado cuando se elige un registro en la lista de PC, pinta en pantalla
     * los campos del seleccionado
     * @param pcModificar
     */
    private void modificarPc(Pc pcModificar) {
        pcModificar.setMarca(vista.txtPcMarca.getText());
        pcModificar.setModelo(vista.txtPcModelo.getText());
        pcModificar.setGaming(vista.checkBoxPcGaming.isSelected());
        pcModificar.setOficina(vista.checkBoxPcOficina.isSelected());
        pcModificar.setStreaming(vista.checkBoxPcStreaming.isSelected());
        pcModificar.setServidor(vista.checkBoxPcServidor.isSelected());
        modelo.guardarPc(pcModificar);
        refrescarListas();
        listarPc();
    }

    /**
     * Es llamado cuando se elige un registro en la lista de piezas, pinta en pantalla
     * los campos del seleccionado
     * @param piezaModificar
     */
    private void modificarPieza(Pieza piezaModificar) {
        piezaModificar.setNombrePieza(vista.txtPiezasNombre.getText());
        piezaModificar.setPrecioPieza(Double.parseDouble(vista.txtPiezasPrecio.getText()));
        piezaModificar.setStock(Integer.parseInt(vista.txtPiezasStock.getText()));
        piezaModificar.setDireccionAlmacen(vista.txtPiezasDireccion.getText());
        piezaModificar.setDescripcion1(vista.txtPiezasDescripcion1.getText());
        piezaModificar.setDescripcion2(vista.txtPiezasDescripcion2.getText());
        piezaModificar.setDescripcion3(vista.txtPiezasDescripcion3.getText());
        piezaModificar.setUrl("\\PCComponentes_Espana\\img\\" + rutaImagen.getName());
        piezaModificar.setProveedor((Proveedor)vista.cbPiezasProveedor.getSelectedItem());
        if (vista.lbPiezasFoto.getIcon() == null) {
            JOptionPane.showConfirmDialog(
                    null,"Selecciona una foto","Error", JOptionPane.ERROR_MESSAGE);
        }else {
            modelo.guardarPieza(piezaModificar);
        }
        refrescarListas();
    }
	/**
	 * Guarda una pieza en la base de datos
	 */
    private void guardarPieza() {
        try {
            modelo.guardarPieza(new Pieza(
                    vista.txtPiezasNombre.getText(),
                    Double.parseDouble(vista.txtPiezasPrecio.getText()),
                    Integer.parseInt(vista.txtPiezasStock.getText()),
                    vista.txtPiezasDireccion.getText(),
                    vista.txtPiezasDescripcion1.getText(),
                    vista.txtPiezasDescripcion2.getText(),
                    vista.txtPiezasDescripcion3.getText(),

                    "\\PCComponentes_Espana\\img\\"+fileChooser.getSelectedFile().getName(),
                    (Proveedor) vista.cbPiezasProveedor.getSelectedItem()
            ));
        }catch (NullPointerException e) {
                    vista.lbPiezasFoto.setIcon(null);
        }
        refrescarListas();
    }
	/**
	 * Guarda una factura en la base de datos
	 */
    private void guardarFactura() {
        if ((vista.dpFacturaFecha.getDate().isAfter(LocalDate.now()))) {
            JOptionPane.showMessageDialog(
                    null,"No se puede dar de alta una factura con fecha superior a la actual",
                    "Error",JOptionPane.ERROR_MESSAGE
            );
        } else {
            modelo.guardarFactura(new Factura(
                    vista.txtFacturaNombre.getText(),
                    vista.txtFacturaDni.getText(),
                    Integer.parseInt(vista.txtFacturaEdad.getText()),
                    Date.valueOf(vista.dpFacturaFecha.getDate()),
                    vista.txtFacturaPago.getText(),
                    vista.txtFacturaEmail.getText(),
                    vista.txtFacturaTelefono.getText(),
                    (Pc) vista.cbFacturaPc.getSelectedItem()
            ));
            refrescarListas();
        }
    }
	/**
	 * Guarda una pieza en la base da datos de un PC seleccionado
	 */
    private void guardarPiezaPc() {
        try {
            Pieza piezaGuardarPc = (Pieza) vista.listPcPiezasDisponibles.getSelectedValue();
            Pc pcGuardarPieza = (Pc) vista.listPc.getSelectedValue();
            pcGuardarPieza.getPiezas().add(piezaGuardarPc);
            modelo.guardarPieza(piezaGuardarPc);
        }catch (NullPointerException e1) {
            modelo.desconectar();
            modelo.conectar();
            JOptionPane.showMessageDialog(null,"Se ha producido un error con la base de datos. Base de datos desconectada y reconectada","ERROR",0);

        }
        listarPcPiezas();
        listarPcPiezasDisponibles();
        refrescarListas();
    }
	/**
	 * Guarda un PC en la base de datos
	 */
    private void guardarPc() {
        modelo.guardarPc(new Pc(
                vista.txtPcMarca.getText(),
                vista.txtPcModelo.getText(),
                vista.checkBoxPcGaming.isSelected(),
                vista.checkBoxPcOficina.isSelected(),
                vista.checkBoxPcStreaming.isSelected(),
                vista.checkBoxPcServidor.isSelected()
        ));
        refrescarListas();
        listarPc();
    }
	/**
	 * Guarda un usuario en la base de datos
	 */
    private void guardarUsuario() {
        try {
            Properties properties = new Properties();
            Libreria libreria = new Libreria();
            properties.load(new FileInputStream("usuarios.txt"));
            properties.setProperty(String.valueOf(vista.listUsuarios.getSelectedValue()),
                    libreria.hashing(String.valueOf(vista.txtUsuariosPassword.getPassword())));
            properties.store(new FileOutputStream("usuarios.txt"), "Archivo de usuarios");
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,"Error al acceder al archivo usuarios.txt","ERROR",0);
        } catch (NoSuchPaddingException e) {
            JOptionPane.showMessageDialog(null,"Error al acceder al archivo usuarios.txt","ERROR",0);
        } catch (NoSuchAlgorithmException e) {
            JOptionPane.showMessageDialog(null,"Error al descifrar en archivo usuarios.txt","ERROR",0);
        }
        JOptionPane.showMessageDialog(null, "Se ha guardado la contraseña nueva",
                "Guardar Usuario", JOptionPane.INFORMATION_MESSAGE);
    }
	
	/**
	 * Elimina una factura seleccionada de la base de datos
	 */
    private void eliminarFactura() {
        Factura facturaEliminar = (Factura) vista.listFactura.getSelectedValue();
        try {
            modelo.eliminarFactura(facturaEliminar);
        }catch (IllegalArgumentException e) {
            modelo.desconectar();
            modelo.conectar();
            JOptionPane.showMessageDialog(null,"Se ha producido un error con la base de datos. Base de datos desconectada y reconectada","ERROR",0);

        }
        refrescarListas();
    }
	/**
	 * Elimina una pieza seleccionada de un PC seleccionado
	 */
    private void eliminarPiezaPc() {
        try {
            Pc pcEliminarPieza = (Pc) vista.listPc.getSelectedValue();
            Pieza piezaEliminarPc = (Pieza) vista.listPcPiezas.getSelectedValue();
            pcEliminarPieza.getPiezas().remove(piezaEliminarPc);
            modelo.guardarPieza(piezaEliminarPc);

        }catch (NullPointerException e1) {
            modelo.desconectar();
            modelo.conectar();
            JOptionPane.showMessageDialog(null,"Se ha producido un error con la base de datos. Base de datos desconectada y reconectada","ERROR",0);

        }
        listarPcPiezas();
        listarPcPiezasDisponibles();
        refrescarListas();
    }
	/**
	 * Elimina un PC seleccionado de la base de datos
	 */
    private void eliminarPc() {
        Pc pcEliminar = (Pc) vista.listPc.getSelectedValue();
        try {
            modelo.eliminarPc(pcEliminar);
        }catch (IllegalArgumentException e) {
            modelo.desconectar();
            modelo.conectar();
            JOptionPane.showMessageDialog(null,"Se ha producido un error con la base de datos. Base de datos desconectada y reconectada","ERROR",0);

        }
        refrescarListas();
        listarPc();
    }
	/**
	* Elimina una pieza seleccionada de la base de datos
	*/
    private void eliminarPieza() {
        Pieza piezaEliminar = (Pieza) vista.listPiezas.getSelectedValue();
        try{
            modelo.eliminarPieza(piezaEliminar);
        }catch (IllegalArgumentException e) {
            modelo.desconectar();
            modelo.conectar();
            JOptionPane.showMessageDialog(null,"Se ha producido un error con la base de datos. Base de datos desconectada y reconectada","ERROR",0);

        }
        refrescarListas();
    }
	/**
	 * Elimina un proveedor seleccionado de la base de datos
	 */
    private void eliminarProveedor() {
        Proveedor proveedorEliminar = (Proveedor) vista.listProveedores.getSelectedValue();
        try {
            modelo.eliminarProveedor(proveedorEliminar);
        } catch (IllegalArgumentException e) {
            modelo.desconectar();
            modelo.conectar();
            JOptionPane.showMessageDialog(null,"Se ha producido un error con la base de datos. Base de datos desconectada y reconectada","ERROR",0);

        }
        refrescarListas();
    }
	
	
	/**
	 * Al cambiar la contrasena te la pide meter dos veces, este metodo comprueba
	 * que se coincidan ambos campos
	 */
    private boolean compararPassword() {
        if (String.valueOf(vista.txtUsuariosPassword.getPassword()).
                equals(String.valueOf(vista.txtUsuariosPassword2.getPassword()))) {
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Las contraseñas no coinciden",
                    "error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
	/**
	 * Pequeña clase que actua de Thread para refrescar todas las listas cada cierto tiempo
	 */
    private class ActualizarTablas implements Runnable {
        @Override
        public void run() {
            while (true) {
                try {
                    refrescarListas();
                    listarPc();
                    //listarPcPiezasDisponibles();
                    Thread.sleep(30000);

                }catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Copia una imagen seleccionada al directorio que corresponde
     * @param source
     */
    private void copy(File source) {
        try {
            FileUtils.copyFileToDirectory(source, new File("\\PCComponentes_Espana\\img\\"));
            //System.out.println("copy");
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,"Error al copiar la foto a la carpeta en C:\\","ERROR",0);

        }
    }

    /**
     * Comprueba que en una string de un textfield no haya dos letras o más
     * @param textField textfield a comprobar
     * @param mensaje mensaje de error para el optionpane
     * @return true si pasa, false si falla la comprobación
     */
    public boolean comprobarNoMasDeDosLetras(JTextField textField, String mensaje) {
        char letra;
        int contador = 0;
        for (int i = 0; i < textField.getText().length(); i++) {
            letra = textField.getText().charAt(i);
            if (Character.isLetter(letra)) {
                contador++;
            }
        }
        if (contador >= 2) {
            JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
            return false;
        } else return true;
    }

    /**
     * Comprueba que haya una letra al final de un string
     * @param textField textfield que comprobar
     * @param mensaje mensaje de error para el optionpane
     * @return true si pasa, false si falla la comprobación
     */
    public boolean comprobarUltimoEsLetra(JTextField textField, String mensaje) {
        if (!Character.isLetter(textField.getText().charAt(textField.getText().length()-1))) {
            JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
            return false;
        } else return true;
    }

    /**
     * Comprueba la longitud de una string
     * @param textField textfield a comprobar
     * @param mensaje mensaje de error del optionpane
     * @param i longitud
     * @return true si pasa, false si falla comprobación
     */
    public boolean comprobarCampoLongitud(JTextField textField, String mensaje, int i) {
        try {
            textField.setText(textField.getText().substring(0, i));
            return true;
        }catch (IndexOutOfBoundsException e1) {
            JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    /**
     * La comprobaciones que pasa la factura antes de ser guardada
     * @return true si pasa, false si falla comprobación
     */
    private boolean comprobarFactura() {
        if (
                comprobarCampoLongitud(vista.txtFacturaDni,"El DNI debe tener 9 caracteres",9) &&
                        comprobarUltimoEsLetra(vista.txtFacturaDni,"El DNI debe tener una letra al final") &&
                        comprobarNoMasDeDosLetras(vista.txtFacturaDni,"El DNI no debe tener más de una letra")
        ) return true;
        else return false;
    }

    /**
     * comprueba y cambia comas por puntos en float
     * @param txt textfield a comprobar
     */
    private void comprobarComaoPunto(JTextField txt) {
        txt.setText(txt.getText().replace("," ,"."));
    }

    /**
     * La comprobación que pasa un proveedor antes de ser guardado
     * @return true si pasa, false si falla comprobación
     */
    private boolean comprobarProveedor() {
        if (
                comprobarNoMasDeDosLetras(vista.txtProveedoresNif,"El NIF no puede tener más de una letra") &&
                        comprobarCampoLongitud(vista.txtProveedoresNif,"El NIF no debe tener más de 9 caracteres",9) &&
                        comprobarUltimoEsLetra(vista.txtProveedoresNif,"El NIF debe acabar por una letra") &&
                        comprobarCampoLongitud(vista.txtProveedoresCodigoPostal,"El CP no debe tener más de 5 caracteres",5)&&
                        comprobarCampoLongitud(vista.txtProveedoresTelefono,"El teléfono no debe tener más de 9 caracteres",9)&&
                        comprobarCampoLongitud(vista.txtProveedoresFax,"El fax no debe tener más de 9 caracteres",9)
        )
            return true;
        else return false;
    }

    /**
     * Según la factura seleccionada, rellena sus datos en los campos por pantalla
     * @param factura factura a mostrar
     */
    private void rellenarFactura(Factura factura) {
        vista.txtFacturaNombre.setText(factura.getNombreFactura());
        vista.txtFacturaDni.setText(factura.getDni());
        vista.txtFacturaEdad.setText(String.valueOf(factura.getEdad()));
        vista.dpFacturaFecha.setDate(factura.getFechaCompra().toLocalDate());
        vista.txtFacturaPago.setText(factura.getMetodoPago());
        vista.txtFacturaEmail.setText(factura.getEmailFactura());
        vista.txtFacturaTelefono.setText(factura.getTelefonoFactura());
        vista.cbFacturaPc.setSelectedItem(factura.getPc());
    }

    /**
     * Según el pc seleccionado, rellena sus datos en los campos por pantalla
     * @param pc pc a mostrar
     */
    private void rellenarPc(Pc pc) {
        vista.txtPcMarca.setText(pc.getMarca());
        vista.txtPcModelo.setText(pc.getModelo());
        vista.checkBoxPcGaming.setSelected(pc.isGaming());
        vista.checkBoxPcOficina.setSelected(pc.isOficina());
        vista.checkBoxPcStreaming.setSelected(pc.isStreaming());
        vista.checkBoxPcServidor.setSelected(pc.isServidor());
        listarPcPiezas();
        listarPcPiezasDisponibles();
    }

    /**
     * Según la pieza seleccionada, rellena sus datos en los campos por pantalla
     * @param pieza pieza a mostrar
     */
    private void rellenarPieza(Pieza pieza) {
        try {
            rutaImagen = new File(pieza.getUrl());
        }catch (NullPointerException e) {
            rutaImagen = null;
        }
        vista.txtPiezasNombre.setText(pieza.getNombrePieza());
        vista.txtPiezasPrecio.setText(String.valueOf(pieza.getPrecioPieza()));
        vista.txtPiezasStock.setText(String.valueOf(pieza.getStock()));
        vista.txtPiezasDireccion.setText(pieza.getDireccionAlmacen());
        vista.txtPiezasDescripcion1.setText(pieza.getDescripcion1());
        vista.txtPiezasDescripcion2.setText(pieza.getDescripcion2());
        vista.txtPiezasDescripcion3.setText(pieza.getDescripcion3());
        vista.lbPiezasFoto.setText("");
        vista.lbPiezasFoto.setIcon(new ImageIcon(new ImageIcon(
                pieza.getUrl()).getImage().getScaledInstance(60,60,Image.SCALE_SMOOTH)));
        vista.cbPiezasProveedor.setSelectedItem(pieza.getProveedor());
    }

    /**
     * Según el proveedor seleccionado, rellena sus datos en los campos por pantalla
     * @param proveedor
     */
    private void rellenarProveedor(Proveedor proveedor) {
        vista.txtProveedoresTelefono.setText(proveedor.getTelefonoEmpresa());
        vista.txtProveedoresNombre.setText(proveedor.getNombreEmpresa().replace("(Proveedor):", ""));
        vista.txtProveedoresNif.setText(proveedor.getNif());
        vista.txtProveedoresFax.setText(proveedor.getFax());
        vista.txtProveedoresEmail.setText(proveedor.getEmailEmpresa());
        vista.txtProveedoresDireccion.setText(proveedor.getDireccionEmpresa());
        vista.txtProveedoresCodigoPostal.setText(proveedor.getCodigoPostalEmpresa());
    }

    /**
     * Rellena la lista de usuarios desde archivo
     * @throws IOException si no se encuentra archivo "usuarios.txt"
     */
    private void rellenarUsuarios() throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("usuarios.txt"));

        vista.txtUsuariosNombre.setText(String.valueOf(vista.listUsuarios.getSelectedValue()));
        vista.txtUsuariosPassword.setText("");
        vista.txtUsuariosPassword2.setText("");
        //vista.txtUsuariosPassword.setText(properties.getProperty(String.valueOf(vista.listUsuarios.getSelectedValue())));
    }

    /**
     * Método que genera el informe de factura
     * @throws SQLException si no se conecta con la base de datos
     */
    private void facturaGenerar() throws SQLException {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conexion = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/pccomponentes_proyecto", "root", ""
            );
            HashMap<String, Object> parametros = new HashMap<String, Object>();
            parametros.put("titulo", "FACTURA SIMPLIFICADA");
            parametros.put("facturaID",((Factura)(vista.listFactura.getSelectedValue())).getId());
            JasperPrint jasperPrint = JasperFillManager.fillReport(
                    "report\\reportProyecto2.jasper",parametros,conexion
            );
            JasperViewer jasperViewer = new JasperViewer(jasperPrint,false);
            jasperViewer.setVisible(true);


        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null,e.getStackTrace(),"ERROR",0);
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que realiza el informe de inventario de la tabla pieza
     * @throws SQLException
     */
    private void informePiezas() throws SQLException {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conexion = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/pccomponentes_proyecto", "root", ""
            );
            JasperPrint jasperPrint = JasperFillManager.fillReport(
                    "report\\reportProyecto.jasper",null,conexion
            );
            JasperViewer jasperViewer = new JasperViewer(jasperPrint,false);
            jasperViewer.setVisible(true);


        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null,e.getStackTrace(),"ERROR",0);

        } catch (JRException e) {
            JOptionPane.showMessageDialog(null,e.getStackTrace(),"ERROR",0);

        }
    }

    /**
     * Método que se ejecuta en el contructor y anade todos los listeners a los diferentes
     * elementos de la interfaz. Si se entra como usuario y no como empleado o admin,
     * no se activan varios.
     */
    public void anadirListeners() {
        //Panel proveedor
        vista.listProveedores.addListSelectionListener(this);
        vista.btProveedoresEliminar.addActionListener(this);
        vista.btProveedoresEliminar.setActionCommand("proveedorEliminar");
        vista.btProveedoresGuardar.addActionListener(this);
        vista.btProveedoresGuardar.setActionCommand("proveedorGuardar");
        vista.btProveedoresModificar.addActionListener(this);
        vista.btProveedoresModificar.setActionCommand("proveedorModificar");
        vista.txtProveedoresCodigoPostal.addKeyListener(this);
        if (!vista.usuario.equals("usuario"))vista.txtProveedoresFax.addKeyListener(this);
        if (!vista.usuario.equals("usuario"))vista.txtProveedoresFax.addFocusListener(this);
        if (!vista.usuario.equals("usuario"))vista.listProveedores.addKeyListener(this);
        if (!vista.usuario.equals("usuario"))vista.txtProveedoresEmail.addFocusListener(this);
        if (!vista.usuario.equals("usuario"))vista.txtProveedoresTelefono.addFocusListener(this);
        if (!vista.usuario.equals("usuario"))vista.txtProveedoresTelefono.addKeyListener(this);
        if (!vista.usuario.equals("usuario"))vista.txtProveedoresCodigoPostal.addFocusListener(this);
        if (!vista.usuario.equals("usuario"))vista.txtProveedoresNif.addFocusListener(this);
        //Panel piezas
        vista.listPiezas.addListSelectionListener(this);
        vista.btPiezasEliminar.addActionListener(this);
        vista.btPiezasEliminar.setActionCommand("piezaEliminar");
        vista.btPiezasGuardar.addActionListener(this);
        vista.btPiezasGuardar.setActionCommand("piezaGuardar");
        vista.btPiezasModificar.addActionListener(this);
        vista.btPiezasModificar.setActionCommand("piezaModificar");
        vista.cbPiezasProveedor.addKeyListener(this);
        vista.btPiezasInforme.addActionListener(this);
        vista.btPiezasInforme.setActionCommand("informePiezas");
        if (!vista.usuario.equals("usuario"))vista.listPiezas.addKeyListener(this);
        if (!vista.usuario.equals("usuario"))vista.txtPiezasPrecio.addKeyListener(this);
        if (!vista.usuario.equals("usuario"))vista.txtPiezasStock.addKeyListener(this);
        if (!vista.usuario.equals("usuario"))vista.lbPiezasFoto.addMouseListener(this);
        vista.txtPiezasPrecio.addFocusListener(this);
        //Panel pc
        vista.listPc.addListSelectionListener(this);
        if (!vista.usuario.equals("usuario"))vista.listPcPiezas.addListSelectionListener(this);
        if (!vista.usuario.equals("usuario"))vista.listPcPiezasDisponibles.addListSelectionListener(this);
        vista.btPcGuardar.addActionListener(this);
        vista.btPcGuardar.setActionCommand("pcGuardar");
        vista.btPcModificar.addActionListener(this);
        vista.btPcModificar.setActionCommand("pcModificar");
        vista.btPcEliminar.addActionListener(this);
        vista.btPcEliminar.setActionCommand("pcEliminar");
        vista.btPcGuardarPieza.addActionListener(this);
        vista.btPcGuardarPieza.setActionCommand("pcGuardarPieza");
        vista.btPcEliminarPieza.addActionListener(this);
        vista.btPcEliminarPieza.setActionCommand("pcEliminarPieza");
        if (!vista.usuario.equals("usuario"))vista.checkBoxPcServidor.addKeyListener(this);
        if (!vista.usuario.equals("usuario"))vista.listPcPiezasDisponibles.addKeyListener(this);
        if (!vista.usuario.equals("usuario"))vista.listPcPiezasDisponibles.addMouseListener(this);
        if (!vista.usuario.equals("usuario"))vista.listPc.addKeyListener(this);
        if (!vista.usuario.equals("usuario"))vista.listPcPiezas.addKeyListener(this);
        if (!vista.usuario.equals("usuario"))vista.listPcPiezas.addMouseListener(this);
        //Panel Factura
        vista.listFactura.addListSelectionListener(this);
        vista.btFacturaGuardar.addActionListener(this);
        vista.btFacturaGuardar.setActionCommand("facturaGuardar");
        vista.btFacturaModificar.addActionListener(this);
        vista.btFacturaModificar.setActionCommand("facturaModificar");
        vista.btFacturaEliminar.addActionListener(this);
        vista.btFacturaEliminar.setActionCommand("facturaEliminar");
        if (!vista.usuario.equals("usuario"))vista.cbFacturaPc.addKeyListener(this);
        if (!vista.usuario.equals("usuario"))vista.listFactura.addKeyListener(this);
        vista.listFactura.addMouseListener(this);
        if (!vista.usuario.equals("usuario"))vista.txtFacturaTelefono.addKeyListener(this);
        vista.txtFacturaEdad.addKeyListener(this);
        vista.txtFacturaTelefono.addKeyListener(this);
        vista.txtFacturaTelefono.addFocusListener(this);
        vista.txtFacturaEdad.addFocusListener(this);
        vista.txtFacturaTelefono.addFocusListener(this);
        vista.txtFacturaDni.addFocusListener(this);
        vista.btFacturaGenerar.addActionListener(this);
        vista.btFacturaGenerar.setActionCommand("facturaGenerar");
        //Panel Usuarios
        vista.listUsuarios.addListSelectionListener(this);
        vista.btUsuariosGuardar.setActionCommand("guardarUsuario");
        vista.btUsuariosGuardar.addActionListener(this);
        vista.btCerrarSesion.setActionCommand("cerrarSesion");
        vista.btCerrarSesion.addActionListener(this);
        //PanelBusqueda
        vista.btBusqueda.setActionCommand("buscar");
        vista.btBusqueda.addActionListener(this);
        vista.listBusqueda.addMouseListener(this);
        vista.txtBusqueda.addKeyListener(this);

    }



}