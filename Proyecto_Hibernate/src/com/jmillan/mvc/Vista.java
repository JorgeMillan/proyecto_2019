package com.jmillan.mvc;

import com.github.lgooddatepicker.components.DatePicker;
import com.jmillan.base.Factura;
import com.jmillan.base.Pc;
import com.jmillan.base.Pieza;
import com.jmillan.base.Proveedor;

import javax.swing.*;

public class Vista {

    Login login;

    static JFrame frame = new JFrame("PCComponentes España");
    String usuario;
    //Componentes de Proveedores
    JTabbedPane tabbedPane1;
    JPanel panel1;
    public JTextField txtProveedoresNombre;
    public JTextField txtProveedoresNif;
    JTextField txtProveedoresDireccion;
    public JTextField txtProveedoresCodigoPostal;
    JTextField txtProveedoresTelefono;
    public JTextField txtProveedoresEmail;
    public JButton btProveedoresGuardar;
    public JButton btProveedoresModificar;
    public JButton btProveedoresEliminar;
    JList listProveedores;
    DefaultListModel<Proveedor> dlmListProveedores;
    public JTextField txtProveedoresFax;

    //Componentes de Piezas
    public JTextField txtPiezasNombre;
    public JTextField txtPiezasPrecio;
    public JTextField txtPiezasStock;
    public JTextField txtPiezasDireccion;
    public JTextField txtPiezasDescripcion1;
    public JTextField txtPiezasDescripcion2;
    public JTextField txtPiezasDescripcion3;
    public JButton btPiezasGuardar;
    public JButton btPiezasModificar;
    public JButton btPiezasEliminar;
    JList listPiezas;
    DefaultListModel<Pieza> dlmListPiezas;
    public JComboBox cbPiezasProveedor;
    DefaultComboBoxModel<Proveedor>dlmCbPiezasProveedor;

    //Componentes de Pc
    public JCheckBox checkBoxPcServidor;
    public JCheckBox checkBoxPcStreaming;
    public JCheckBox checkBoxPcOficina;
    public JCheckBox checkBoxPcGaming;
    public JTextField txtPcMarca;
    public JTextField txtPcModelo;
    JList listPc;
    DefaultListModel<Pc> dlmListPc;
    public JButton btPcEliminar;
    public JButton btPcGuardar;
    public JButton btPcModificar;
    JList listPcPiezas;
    DefaultListModel<Pieza> dlmListPcPieza;
    JList listPcPiezasDisponibles;
    DefaultListModel<Pieza> dlmListPcPiezasDisponibles;
    public JButton btPcEliminarPieza;
    public JButton btPcGuardarPieza;

    //Facturas
    public JTextField txtFacturaNombre;
    public JTextField txtFacturaDni;
    public JTextField txtFacturaEdad;
    public JTextField txtFacturaPago;
    public JTextField txtFacturaEmail;
    public JTextField txtFacturaTelefono;
    public JComboBox cbFacturaPc;
    DefaultComboBoxModel<Pc> dlmCbFacturaPc;
    JList listFactura;
    DefaultListModel<Factura> dlmListFactura;
    public JButton btFacturaGuardar;
    public JButton btFacturaModificar;
    public DatePicker dpFacturaFecha;
    public JButton btFacturaEliminar;

    //Usuarios
    public JTextField txtUsuariosNombre;
    public JPasswordField txtUsuariosPassword;
    public JButton btUsuariosGuardar;
    JList listUsuarios;
    public JButton btCerrarSesion;
    public JPasswordField txtUsuariosPassword2;
    DefaultListModel<String> dlmListUsuarios;

    //Busqueda
    JRadioButton rbBusquedaFacturas;
    JRadioButton rbBusquedaPc;
    JRadioButton rbBusquedaPiezas;
    JRadioButton rbBusquedaProveedores;
    ButtonGroup busqueda;
    public JTextField txtBusqueda;
    public JButton btBusqueda;
    JList listBusqueda;
    public JLabel lbPiezasFoto;
    JButton btFacturaGenerar;
    JButton btPiezasInforme;
    DefaultListModel<Proveedor> dlmListBusquedaProveedor;
    DefaultListModel<Pieza> dlmListBusquedaPieza;
    DefaultListModel<Pc> dlmListBusquedaPc;
    DefaultListModel<Factura> dlmListBusquedaFactura;


    public Vista(Login login, String usuario) {

        this.login = login;
        this.usuario = usuario;

        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(300,20);
        frame.setSize(700,700);

        setPermisos();
        setDlm();
        //setToolkits();
        listProveedores.setSelectionMode(0);
        listPiezas.setSelectionMode(0);
        listPc.setSelectionMode(0);
        listPcPiezas.setSelectionMode(0);
        listPcPiezasDisponibles.setSelectionMode(0);
        listFactura.setSelectionMode(0);
        //txtFacturaTelefono = new JFormattedTextField(new Integer(15));
        listUsuarios.setSelectionMode(0);

        busqueda = new ButtonGroup();
        busqueda.add(rbBusquedaFacturas);
        busqueda.add(rbBusquedaPc);
        busqueda.add(rbBusquedaPiezas);
        busqueda.add(rbBusquedaProveedores);

    }

    /**
     * Con este LookAndFeel aplicado, la JVM crashea si los pongo, así que aquí están sin hacer nada
     */
    private void setToolkits() {
        //Panel premisos
        txtUsuariosNombre.setToolTipText("Nivel");
        txtUsuariosPassword.setToolTipText("Introduzca la nueva contraseña");
        txtUsuariosPassword2.setToolTipText("Introduzca la nueva contraseña otra vez");
        btUsuariosGuardar.setToolTipText("Guardar");
        btCerrarSesion.setToolTipText("Cerrar Sesión");
        txtBusqueda.setToolTipText("Texto a buscar");
        btBusqueda.setToolTipText("Buscar");
        //panel proveedores
        txtProveedoresNombre.setToolTipText("Nombre proveedor");
        txtProveedoresNif.setToolTipText("NIF proveedor");
        txtProveedoresCodigoPostal.setToolTipText("CP proveedor");
        txtFacturaTelefono.setToolTipText("Teléfono proveedor");
        txtProveedoresEmail.setToolTipText("E-mail proveedor");
        txtProveedoresFax.setToolTipText("Fax proveedor");
        btProveedoresGuardar.setToolTipText("Guardar");
        btProveedoresModificar.setToolTipText("Modificar");
        btProveedoresEliminar.setToolTipText("Eliminar");
        //Panel piezas
        lbPiezasFoto.setToolTipText("Imagen pieza");
        txtPiezasNombre.setToolTipText("Nombre pieza");
        txtPiezasPrecio.setToolTipText("Precio pieza");
        txtPiezasStock.setToolTipText("Stock");
        txtPiezasDireccion.setToolTipText("Direccion pieza");
        txtPiezasDescripcion1.setToolTipText("Descripción de la pieza");
        txtPiezasDescripcion2.setToolTipText("Descripción de la pieza");
        txtPiezasDescripcion3.setToolTipText("Descripción de la pieza");
        cbPiezasProveedor.setToolTipText("Proveedor de la pieza");
        btPiezasGuardar.setToolTipText("Guardar");
        btPiezasModificar.setToolTipText("Modificar");
        btPiezasEliminar.setToolTipText("Elimnar");
        //Panel pc
        txtPcMarca.setToolTipText("Marca pc");
        txtPcModelo.setToolTipText("Modelo pc");
        checkBoxPcGaming.setToolTipText("Indicador gaming pc");
        checkBoxPcOficina.setToolTipText("Indicador oficina pc");
        checkBoxPcStreaming.setToolTipText("Indicador streaming pc");
        checkBoxPcServidor.setToolTipText("Indiciador servidor pc");
        btPcGuardar.setToolTipText("Guardar");
        btPcModificar.setToolTipText("Modificar");
        btPcEliminar.setToolTipText("Eliminar");
        btPcEliminarPieza.setToolTipText("Eliminar pieza pc");
        btPcGuardarPieza.setToolTipText("Guardar pieza pc");
        //panel factura
        txtFacturaNombre.setToolTipText("Nombre factura");
        txtFacturaDni.setToolTipText("DNI factura");
        txtFacturaEdad.setToolTipText("Edad factura");
        dpFacturaFecha.setToolTipText("Fecha factura");
        txtFacturaPago.setToolTipText("Método de pago factura");
        txtFacturaEmail.setToolTipText("E-mail factura");
        txtFacturaTelefono.setToolTipText("Teléfono factura");
        cbFacturaPc.setToolTipText("Pc factura");
        btFacturaGuardar.setToolTipText("Guardar factura");
        btFacturaModificar.setToolTipText("Modificar factura");
        btFacturaEliminar.setToolTipText("Eliminar factura");

    }

    private void setPermisos() {
        switch (login.usuario) {
            case "usuario":
                txtFacturaNombre.setEditable(false);
                txtFacturaDni.setEditable(false);
                txtFacturaEdad.setEditable(false);
                dpFacturaFecha.setEnabled(false);
                txtFacturaPago.setEditable(false);
                txtFacturaEmail.setEditable(false);
                txtFacturaTelefono.setEditable(false);
                cbFacturaPc.setEnabled(false);
                btFacturaGuardar.setVisible(false);
                btFacturaModificar.setVisible(false);
                btFacturaEliminar.setVisible(false);

                txtPcMarca.setEditable(false);
                txtPcModelo.setEditable(false);
                checkBoxPcGaming.setEnabled(false);
                checkBoxPcOficina.setEnabled(false);
                checkBoxPcStreaming.setEnabled(false);
                checkBoxPcServidor.setEnabled(false);
                btPcGuardar.setVisible(false);
                btPcModificar.setVisible(false);
                btPcEliminar.setVisible(false);
                btPcEliminarPieza.setVisible(false);
                btPcGuardarPieza.setVisible(false);
                listPcPiezasDisponibles.setEnabled(false);

                txtPiezasNombre.setEditable(false);
                txtPiezasPrecio.setEditable(false);
                txtPiezasStock.setEditable(false);
                txtPiezasDireccion.setEditable(false);
                txtPiezasDescripcion1.setEditable(false);
                txtPiezasDescripcion2.setEditable(false);
                txtPiezasDescripcion3.setEditable(false);
                cbPiezasProveedor.setEnabled(false);
                btPiezasGuardar.setVisible(false);
                btPiezasModificar.setVisible(false);
                btPiezasEliminar.setVisible(false);

                txtProveedoresNombre.setEditable(false);
                txtProveedoresNif.setEditable(false);
                txtProveedoresDireccion.setEditable(false);
                txtProveedoresCodigoPostal.setEditable(false);
                txtProveedoresTelefono.setEditable(false);
                txtProveedoresEmail.setEditable(false);
                txtProveedoresFax.setEditable(false);
                btProveedoresGuardar.setVisible(false);
                btProveedoresModificar.setVisible(false);
                btProveedoresEliminar.setVisible(false);
                break;

        }
    }
    public void visible() {
        if (login.acceder) frame.setVisible(true);
    }
    private void setDlm() {
        //dlm Proveedores
        dlmListProveedores = new DefaultListModel<>();
        listProveedores.setModel(dlmListProveedores);
        listProveedores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        //dlmPiezas
        dlmListPiezas = new DefaultListModel<>();
        listPiezas.setModel(dlmListPiezas);
        listPiezas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        dlmCbPiezasProveedor = new DefaultComboBoxModel<>();
        cbPiezasProveedor.setModel(dlmCbPiezasProveedor);

        //dlm Pc
        dlmListPc = new DefaultListModel<>();
        listPc.setModel(dlmListPc);
        dlmListPcPieza = new DefaultListModel<>();
        listPcPiezas.setModel(dlmListPcPieza);
        dlmListPcPiezasDisponibles = new DefaultListModel<>();
        listPcPiezasDisponibles.setModel(dlmListPcPiezasDisponibles);

        //dlm Factura
        dlmListFactura = new DefaultListModel<>();
        listFactura.setModel(dlmListFactura);
        dlmCbFacturaPc = new DefaultComboBoxModel<>();
        cbFacturaPc.setModel(dlmCbFacturaPc);

        //dlm usuarios
        dlmListUsuarios = new DefaultListModel<String>();
        listUsuarios.setModel(dlmListUsuarios);
    }
}
