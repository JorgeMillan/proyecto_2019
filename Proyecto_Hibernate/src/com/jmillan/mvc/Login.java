package com.jmillan.mvc;

import com.jmillan.Libreria.Libreria;

import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

public class Login {
    private JPanel panel1;
    private JTextField txtUsuario;
    private JPasswordField txtPassword;
    private JButton accederButton;
    static boolean acceder = false;
    String usuario;
    JFrame frame = new JFrame("Log In");
    Libreria libreria;

    {
        try {
            libreria = new Libreria();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

	/**
	 * Constructor de la clase
	 */
    public Login() {
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,150);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        //cargarArchivo();

        /**
         * Listener del botón de acceso
         */
        accederButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    comprobar();
                } catch (NoSuchAlgorithmException e1) {
                    e1.printStackTrace();
                }
            }
        });

        /**
         * KeyListener del txt de password para entrar con intro
         */
        txtPassword.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {}
            @Override
            public void keyPressed(KeyEvent e) {}

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    try {
                        comprobar();
                    } catch (NoSuchAlgorithmException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }

	/**
	* Comprueba con el archivo de usuarios si coincida para dar acceso a la aplicacion o no.
	* @throws NoSuchAlgorithmException Si falla la encriptacion.
	*/
    private void comprobar() throws NoSuchAlgorithmException {

        File saves = new File("usuarios.txt");
        if (!saves.exists()) {
            cargarArchivo();
        }

        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("usuarios.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (libreria.hashing(String.valueOf(txtPassword.getPassword())).equals(
                properties.getProperty(txtUsuario.getText()))) {
            acceder = true;
            frame.dispose();
            usuario = txtUsuario.getText();
           //Vista.visible();
           Vista vista = new Vista(this, usuario);
           Modelo modelo = new Modelo();
           Controlador controlador = new Controlador(vista,modelo);
           vista.visible();
        } else {
            acceder = false;
            JOptionPane.showMessageDialog(null,"Error en usuario o contraseña",
                    "Log In", JOptionPane.ERROR_MESSAGE);
        }
    }

	/**
	* Crea el archivo de usuarios si no existe
	*/
    private void cargarArchivo() {
        Properties properties = new Properties();
        File usuarios = new File("usuarios.txt");
        try {
            if (!usuarios.exists()) {
                new FileWriter("usuarios.txt");
                properties.load(new FileInputStream("usuarios.txt"));
                properties.setProperty("admin", libreria.hashing("admin"));
                properties.setProperty("empleado", libreria.hashing("empleado"));
                properties.setProperty("usuario", libreria.hashing("usuario"));
                properties.store(new FileOutputStream("usuarios.txt"), "Fichero de usuarios");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}


