package com.jmillan.base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "pc")
public class Pc {
    private int id;
    private String marca;
    private String modelo;
    private boolean gaming;
    private boolean oficina;
    private boolean streaming;
    private boolean servidor;
    private List<Pieza> piezas;
    private List<Factura> facturas;

    public Pc() {
    }

    public Pc(String marca, String modelo, boolean gaming, boolean oficina, boolean streaming, boolean servidor) {
        this.marca = marca;
        this.modelo = modelo;
        this.gaming = gaming;
        this.oficina = oficina;
        this.streaming = streaming;
        this.servidor = servidor;
        piezas = new ArrayList<>();
        facturas = new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "marca")
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Basic
    @Column(name = "modelo")
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Basic
    @Column(name = "gaming")
    public boolean isGaming() {
        return gaming;
    }

    public void setGaming(boolean gaming) {
        this.gaming = gaming;
    }

    @Basic
    @Column(name = "oficina")
    public boolean isOficina() {
        return oficina;
    }

    public void setOficina(boolean oficina) {
        this.oficina = oficina;
    }

    @Basic
    @Column(name = "streaming")
    public boolean isStreaming() {
        return streaming;
    }

    public void setStreaming(boolean streaming) {
        this.streaming = streaming;
    }

    @Basic
    @Column(name = "servidor")
    public boolean isServidor() {
        return servidor;
    }

    public void setServidor(boolean servidor) {
        this.servidor = servidor;
    }

    @Override
    public String toString() {
        return marca + " " + modelo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pc pc = (Pc) o;
        return id == pc.id &&
                gaming == pc.gaming &&
                oficina == pc.oficina &&
                streaming == pc.streaming &&
                servidor == pc.servidor &&
                Objects.equals(marca, pc.marca) &&
                Objects.equals(modelo, pc.modelo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, marca, modelo, gaming, oficina, streaming, servidor);
    }

    @ManyToMany
    @JoinTable(name = "pc_pieza", catalog = "", schema = "pccomponentes_proyecto", joinColumns = @JoinColumn(name = "id_pc", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_pieza", referencedColumnName = "id_pieza", nullable = false))
    public List<Pieza> getPiezas() {
        return piezas;
    }

    public void setPiezas(List<Pieza> piezas) {
        this.piezas = piezas;
    }

    @OneToMany(mappedBy = "pc")
    public List<Factura> getFacturas() {
        return facturas;
    }

    public void setFacturas(List<Factura> facturas) {
        this.facturas = facturas;
    }
}
