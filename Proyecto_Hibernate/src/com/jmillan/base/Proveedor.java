package com.jmillan.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "proveedores", schema = "pccomponentes_proyecto", catalog = "")
public class Proveedor {
    private int id;
    private String nombreEmpresa;
    private String nif;
    private String direccionEmpresa;
    private String codigoPostalEmpresa;
    private String telefonoEmpresa;
    private String emailEmpresa;
    private String fax;
    private List<Pieza> piezas;

    public Proveedor() {
    }

    public Proveedor(String nombreEmpresa, String nif, String direccionEmpresa, String codigoPostalEmpresa, String telefonoEmpresa, String emailEmpresa, String fax) {
        this.nombreEmpresa = nombreEmpresa;
        this.nif = nif;
        this.direccionEmpresa = direccionEmpresa;
        this.codigoPostalEmpresa = codigoPostalEmpresa;
        this.telefonoEmpresa = telefonoEmpresa;
        this.emailEmpresa = emailEmpresa;
        this.fax = fax;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_empresa")
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    @Basic
    @Column(name = "nif")
    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    @Basic
    @Column(name = "direccion_empresa")
    public String getDireccionEmpresa() {
        return direccionEmpresa;
    }

    public void setDireccionEmpresa(String direccionEmpresa) {
        this.direccionEmpresa = direccionEmpresa;
    }

    @Basic
    @Column(name = "codigo_postal_empresa")
    public String getCodigoPostalEmpresa() {
        return codigoPostalEmpresa;
    }

    public void setCodigoPostalEmpresa(String codigoPostalEmpresa) {
        this.codigoPostalEmpresa = codigoPostalEmpresa;
    }

    @Basic
    @Column(name = "telefono_empresa")
    public String getTelefonoEmpresa() {
        return telefonoEmpresa;
    }

    public void setTelefonoEmpresa(String telefonoEmpresa) {
        this.telefonoEmpresa = telefonoEmpresa;
    }

    @Basic
    @Column(name = "email_empresa")
    public String getEmailEmpresa() {
        return emailEmpresa;
    }

    public void setEmailEmpresa(String emailEmpresa) {
        this.emailEmpresa = emailEmpresa;
    }

    @Basic
    @Column(name = "fax")
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }


    /*
    Método toString para presentar en la aplicación, como al dar
    click en una lista ya se muestra t o d o en los campos de arriba,
    solo enseña el nombre
     */
    @Override
    public String toString() {
        return nombreEmpresa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proveedor proveedor = (Proveedor) o;
        return id == proveedor.id &&
                Objects.equals(nombreEmpresa, proveedor.nombreEmpresa) &&
                Objects.equals(nif, proveedor.nif) &&
                Objects.equals(direccionEmpresa, proveedor.direccionEmpresa) &&
                Objects.equals(codigoPostalEmpresa, proveedor.codigoPostalEmpresa) &&
                Objects.equals(telefonoEmpresa, proveedor.telefonoEmpresa) &&
                Objects.equals(emailEmpresa, proveedor.emailEmpresa) &&
                Objects.equals(fax, proveedor.fax);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreEmpresa, nif, direccionEmpresa, codigoPostalEmpresa, telefonoEmpresa, emailEmpresa, fax);
    }

    @OneToMany(mappedBy = "proveedor")
    public List<Pieza> getPiezas() {
        return piezas;
    }

    public void setPiezas(List<Pieza> piezas) {
        this.piezas = piezas;
    }
}
