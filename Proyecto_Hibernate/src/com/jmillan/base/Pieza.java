package com.jmillan.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "piezas", schema = "pccomponentes_proyecto", catalog = "")
public class Pieza {
    private int id;
    private String nombrePieza;
    private double precioPieza;
    private int stock;
    private String direccionAlmacen;
    private String descripcion1;
    private String descripcion2;
    private String descripcion3;
    private String url;
    private Proveedor proveedor;
    private List<Pc> pcs_pieza;

    public Pieza() {
    }

    public Pieza(String nombrePieza, double precioPieza, int stock, String direccionAlmacen, String descripcion1, String descripcion2, String descripcion3, String url, Proveedor proveedor) {
        this.nombrePieza = nombrePieza;
        this.precioPieza = precioPieza;
        this.stock = stock;
        this.direccionAlmacen = direccionAlmacen;
        this.descripcion1 = descripcion1;
        this.descripcion2 = descripcion2;
        this.descripcion3 = descripcion3;
        this.url = url;
        this.proveedor = proveedor;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id_pieza")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_pieza")
    public String getNombrePieza() {
        return nombrePieza;
    }

    public void setNombrePieza(String nombrePieza) {
        this.nombrePieza = nombrePieza;
    }

    @Basic
    @Column(name = "precio_pieza")
    public double getPrecioPieza() {
        return precioPieza;
    }

    public void setPrecioPieza(double precioPieza) {
        this.precioPieza = precioPieza;
    }

    @Basic
    @Column(name = "stock")
    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Basic
    @Column(name = "direccion_almacen")
    public String getDireccionAlmacen() {
        return direccionAlmacen;
    }

    public void setDireccionAlmacen(String direccionAlmacen) {
        this.direccionAlmacen = direccionAlmacen;
    }

    @Basic
    @Column(name = "descripcion1")
    public String getDescripcion1() {
        return descripcion1;
    }

    public void setDescripcion1(String descripcion1) {
        this.descripcion1 = descripcion1;
    }

    @Basic
    @Column(name = "descripcion2")
    public String getDescripcion2() {
        return descripcion2;
    }

    public void setDescripcion2(String descripcion2) {
        this.descripcion2 = descripcion2;
    }

    @Basic
    @Column(name = "descripcion3")
    public String getDescripcion3() {
        return descripcion3;
    }

    public void setDescripcion3(String descripcion3) {
        this.descripcion3 = descripcion3;
    }

    @Basic
    @Column(name = "urlFoto")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return nombrePieza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pieza pieza = (Pieza) o;
        return id == pieza.id &&
                Double.compare(pieza.precioPieza, precioPieza) == 0 &&
                stock == pieza.stock &&
                Objects.equals(nombrePieza, pieza.nombrePieza) &&
                Objects.equals(direccionAlmacen, pieza.direccionAlmacen) &&
                Objects.equals(descripcion1, pieza.descripcion1) &&
                Objects.equals(descripcion2, pieza.descripcion2) &&
                Objects.equals(descripcion3, pieza.descripcion3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombrePieza, precioPieza, stock, direccionAlmacen, descripcion1, descripcion2, descripcion3);
    }

    @ManyToOne
    @JoinColumn(name = "id_proveedor", referencedColumnName = "id")
    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    @ManyToMany
    @JoinTable(name = "pc_pieza", catalog = "", schema = "pccomponentes_proyecto", joinColumns = @JoinColumn(name = "id_pieza", referencedColumnName = "id_pieza", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_pc", referencedColumnName = "id", nullable = false))
    public List<Pc> getPcs_pieza() {
        return pcs_pieza;
    }

    public void setPcs_pieza(List<Pc> pcs_pieza) {
        this.pcs_pieza = pcs_pieza;
    }
}
