package com.jmillan.base;

import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.*;
import java.sql.Date;
import java.util.Comparator;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Factura {
    private int id;
    private String nombreFactura;
    private String dni;
    private int edad;
    private Date fechaCompra;
    private String metodoPago;
    private String emailFactura;
    private String telefonoFactura;
    private Pc pc;


    public Factura(String nombreFactura, String dni, int edad, Date fechaCompra, String metodoPago, String emailFactura, String telefonoFactura, Pc pc) {
        this.nombreFactura = nombreFactura;
        this.dni = dni;
        this.edad = edad;
        this.fechaCompra = fechaCompra;
        this.metodoPago = metodoPago;
        this.emailFactura = emailFactura;
        this.telefonoFactura = telefonoFactura;
        this.pc = pc;
    }

    public Factura() {
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_cliente")
    @ColumnTransformer(read = "aes_decrypt(nombre_cliente, 'contraseña')", write = "aes_encrypt(?, 'contraseña')")
    public String getNombreFactura() {
        return nombreFactura;
    }

    public void setNombreFactura(String nombreFactura) {
        this.nombreFactura = nombreFactura;
    }

    @Basic
    @Column(name = "dni")
    @ColumnTransformer(read = "aes_decrypt(dni, 'contraseña2')", write = "aes_encrypt(?, 'contraseña2')")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "edad")
    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Basic
    @Column(name = "fecha_compra")
    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    @Basic
    @Column(name = "metodo_pago")
    //@ColumnTransformer(read = "aes_decrypt(metodo_pago, 'contraseña3')", write = "aes_encrypt(?, 'contraseña3')")
    public String getMetodoPago() {
        return metodoPago;
    }

    public void setMetodoPago(String metodoPago) {
        this.metodoPago = metodoPago;
    }

    @Basic
    @Column(name = "email_cliente")
    @ColumnTransformer(read = "aes_decrypt(email_cliente, 'contraseña4')", write = "aes_encrypt(?, 'contraseña4')")
    public String getEmailFactura() {
        return emailFactura;
    }

    public void setEmailFactura(String emailFactura) {
        this.emailFactura = emailFactura;
    }

    @Basic
    @Column(name = "telefono_cliente")
    @ColumnTransformer(read = "aes_decrypt(telefono_cliente, 'contraseña5')", write = "aes_encrypt(?, 'contraseña5')")
    public String getTelefonoFactura() {
        return telefonoFactura;
    }

    public void setTelefonoFactura(String telefonoFactura) {
        this.telefonoFactura = telefonoFactura;
    }

    @Override
    public String toString() {
        return nombreFactura + " (" + dni + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Factura factura = (Factura) o;
        return id == factura.id &&
                edad == factura.edad &&
                Objects.equals(nombreFactura, factura.nombreFactura) &&
                Objects.equals(dni, factura.dni) &&
                Objects.equals(fechaCompra, factura.fechaCompra) &&
                Objects.equals(metodoPago, factura.metodoPago) &&
                Objects.equals(emailFactura, factura.emailFactura) &&
                Objects.equals(telefonoFactura, factura.telefonoFactura);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreFactura, dni, edad, fechaCompra, metodoPago, emailFactura, telefonoFactura);
    }

    @ManyToOne
    @JoinColumn(name = "id_pc", referencedColumnName = "id")
    public Pc getPc() {
        return pc;
    }

    public void setPc(Pc pc) {
        this.pc = pc;
    }


    public static Comparator<Factura> comparator = new Comparator<Factura>() {

        @Override
        public int compare(Factura o1, Factura o2) {
            String nombre1 = o1.getNombreFactura();
            String nombre2 = o2.getNombreFactura();
            return nombre1.compareTo(nombre2);
        }
    };
}
