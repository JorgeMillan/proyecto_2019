package com.jmillan.test;

import com.jmillan.mvc.Controlador;
import com.jmillan.mvc.Login;
import com.jmillan.mvc.Modelo;
import com.jmillan.mvc.Vista;
import org.jfree.text.TextBox;
import org.junit.jupiter.api.*;

import javax.swing.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

public class Tests {
    static Controlador controlador;
    static JTextField txt;
    static Modelo modelo;
    static Connection conexion;

    @BeforeEach
    public void beforeEach() {
        txt = new JTextField();
        controlador = new Controlador();
        modelo = new Modelo();
        conexion = null;

    }

    //Testea con jdbc que se conecta a la base de datos, esta conexión por jdbc se usa para los informes
    @Test
    public void pruebaConexion() {
        try {
            Connection conexion = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/pccomponentes_proyecto", "root", ""
            );
        } catch (SQLException e) {
            fail();
        }
    }

    //Testea que de error y salte la excepcion parámetros incorrectos
    @Test
    public void pruebaConexionFallidaNombreIncorrecto() {
        try {
            Connection conexion = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/baseDeDatosIncorrecta", "root", ""
            );
            fail();
        } catch (SQLException e) {
            assertTrue(e.getErrorCode() != 0);
        }
    }
    @Test
    public void pruebaConexionFallidaPasswordIncorrecto() {
        try {
            Connection conexion = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/pccomponentes_proyecto", "root", "passwordIncorrecto"
            );
            fail();
        } catch (SQLException e) {
            assertTrue(e.getErrorCode() != 0);
        }
    }
    @Test
    public void pruebaConexionFallidaUsuarioIncorrecto() {
        try {
            Connection conexion = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/pccomponentes_proyecto", "usuarioIncorrecto", ""
            );
            fail();
        } catch (SQLException e) {
            assertTrue(e.getErrorCode() != 0);
        }
    }

    //Testea si salta el error al recortar algo más pequeño de nueve posiciones
    @Test
    public void pruebaDNI_NIFLongitudMenos() {
        txt.setText("123456");
        controlador.comprobarCampoLongitud(txt,"Funciona el recortar",9);
    }
    //Testea si recorta bien el método
    @Test
    public void pruebaDNI_NIFLongitudMas() {
        txt.setText("12345678910");
        controlador.comprobarCampoLongitud(txt,"No funciona",9);
        assertEquals("123456789", txt.getText());
    }
    //Testea si salta el error al no poner una letra al final
    @Test
    public void pruebaDNI_NIFLetraFinal() {
        txt.setText("123212457");
        controlador.comprobarUltimoEsLetra(txt,"Funciona comprobar letra al final");
    }
    //Testea que reconozca bien la letra al final
    @Test
    public void pruebaDNI_NIFLetraFinal2() {
        txt.setText("12321245b");
        controlador.comprobarUltimoEsLetra(txt,"No Funciona");
    }
    //Testea que de error al ver más de una letra en el dni/nif
    @Test
    public void pruebaDNI_NIFLetras() {
        txt.setText("123s456d82");
        controlador.comprobarNoMasDeDosLetras(txt,"Funciona contar letras nif-dni");
    }
    //Testea que no de error este método cuando hay solo una letra
    @Test
    public void pruebaDNI_NIFLetras2() {
        txt.setText("1234f6789");
        controlador.comprobarNoMasDeDosLetras(txt,"No Funciona");
    }
}










/*Los métodos que se están probando, para mo tener que ir al Controlador a mirar lo que hacen

    public void comprobarNoMasDeDosLetras(JTextField textField, String mensaje) {
        char letra;
        int contador = 0;
        for (int i = 0; i < textField.getText().length(); i++) {
            letra = textField.getText().charAt(i);
            if (Character.isLetter(letra)) {
                contador++;
            }
        }
        if (contador >= 2) {
            JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
        }
    }

    public void comprobarUltimoEsLetra(JTextField textField, String mensaje) {
        if (!Character.isLetter(textField.getText().charAt(textField.getText().length()-1))) {
            JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
        }
    }

    public void comprobarCampoLongitud(JTextField textField, String mensaje, int i) {
        try {
            textField.setText(textField.getText().substring(0, 9));
        }catch (IndexOutOfBoundsException e1) {
            JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
        }
    }
*/

